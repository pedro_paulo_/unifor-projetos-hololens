﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JsonManagerApodi : MonoBehaviour
{
    #region Test

    #endregion

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public static class JsonHelper
    {
        public static T[] FromJson<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.Items;
        }

        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper);
        }

        public static string ToJson<T>(T[] array, bool prettyPrint)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper, prettyPrint);
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }
    }
    [Serializable]
    public class Component
    {
        public int id;
        public string code;
        public string name;
        public int order;
        public dynamic parent_id;
        public Component[] subsubComponents;
    }

    [Serializable]
    public class Maquina
    {
        public dynamic id;
        public dynamic code;
        public dynamic name;
        public dynamic order;
        public dynamic parent_id;
        public dynamic[] subComponents;

        public Maquina(dynamic id, dynamic code, dynamic name, dynamic order, dynamic parent_id)
        {
            this.id = id;
            this.code = code;
            this.name = name;
            this.order = order;
            this.parent_id = parent_id;
            this.subComponents = subComponents;
        }
    }

    [Serializable]
    public class AlertedParameters
    {
        public int id;
        public string code;
        public string name;
        public string unit;
        public string displayFormat;
        public dynamic threshold_hh;
        public dynamic threshold_h;
        public dynamic threshold_ll;
        public dynamic threshold_l;
        public string alerted;
        public dynamic lastUpdate;
        public string componentId;
        public dynamic[] values;

    }


    [Serializable]
    public class Parametro
    {
        public int id;
        public string code;
        public string name;
        public string unit;
        public dynamic displayFormat;
        public dynamic threshold_hh;
        public dynamic threshold_h;
        public dynamic threshold_ll;
        public dynamic threshold_l;
        public string alerted;
        public dynamic lastUpdate;
        public int component_id;
        public int order;
        public dynamic[] values;

    }

    [Serializable]
    public class Equipment
    {
        public int id;
        public string code;
        public string name;
        public Parametro[] parameters;
    }


}

