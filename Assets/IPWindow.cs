﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class IPWindow : MonoBehaviour
{   
    public TMP_InputField ip;
    public TMP_InputField port;

    public TouchScreenKeyboard keyboard;

    bool end = false;

    GameObject ca;

    // Start is called before the first frame update
    void Start()
    {
        ca = GameObject.Find("Screens");
    }

    public void CloseIPWindow()
    {
        end = true;
    }

    public void StartIPWindow()
    {
        if (end == true)
        {
            end = false;
            this.gameObject.SetActive(true);
        }
        else
        {
            end = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (end)
        {
            this.gameObject.SetActive(false);
        }
            
    }

    public void OpenKeyboardIP()
    {
        keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, false, false, false);
    }

    public void SaveIP()
    {
        ip.text = keyboard.text;
    }

    public void OpenKeyboardPort()
    {
        keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, false, false, false);
    }

    public void SavePort()
    {
        port.text = keyboard.text;
    }

    public void AtualizarIPPorta()
    {
        if (ip.text != null && port.text != null)
        {
            string ipPort = ip.text + ":" + port.text;
            ca.GetComponent<ClientApodi>().UpdateUrl(ipPort);
        }
    }
}
