﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using UnityEngine.SceneManagement;

public class ClientApodi : MonoBehaviour
{

    public Text message;
    public GameObject machine;
    public InputField textInputField;
    float targetTime = 60.0f;


    string CurrentText;

    ParameterManager pr;
    List<JsonManagerApodi.Parametro> listaParametros;
    List<JsonManagerApodi.AlertedParameters> listaAlertas;
    List<JsonManagerApodi.Maquina> listaMaquinas;

    public Text URLTEXT;
    GameObject controleQR;

    #region Elementos Fixos
    [SerializeField]
    private GameObject parametrosPrefab;

    [SerializeField]
    private GameObject paramList;

    public string qrCode = "";

    #endregion

    #region URL's
    string urlRoot = "http://172.18.9.227:8085";
    string getURLbyQRCode = "/components/qrcode/";
    string postURL = "";
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        message.text = "Scan para iniciar";
        machine.SetActive(false);
        getURLbyQRCode = urlRoot + getURLbyQRCode;

        controleQR = GameObject.Find("QRCodeCameraManager");

        paramList = GameObject.Find("Content");

        GetParametrosByQRCode("CY01");
    }


    public void UpdateUrl(string url)
    {
        //urlRoot = "http://" + url;
        //Debug.Log(urlRoot);
    }

    public void GetParametrosByQRCode(string qr)
    {
        
        //Debug.Log(qr);
        message.text = "atualizando dados...";
        StartCoroutine(GetParametrosRequest(qr));
        if (machine.activeSelf == false) {
            machine.SetActive(true);   
        }

    }

    public void GetAlarmes()
    {
        message.text = "atualizando dados...";
        StartCoroutine(GetAlarmesRequest());
    }

    public void GetAllComponents()
    {
        message.text = "atualizando dados...";
        StartCoroutine(GetAllComponentsRequest());
    }

    // Update is called once per frame
    void Update()
    {
        targetTime -= Time.deltaTime;
        if (targetTime == 60.0f)
        {
            UpdateList();
        }
        else if (targetTime <= 0.0f)
        {
            targetTime = 60.0f;
        }

        if (qrCode != "")
        {
            GetParametrosByQRCode(qrCode);
        }

    }

    void UpdateList()
    {
        message.text = CurrentText;

        foreach (Transform child in paramList.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        if (listaParametros != null)
        {
            //qRCode.OnReset();
            for (int i = 0; i < listaParametros.Count; i++)
            {
                GameObject go = Instantiate(parametrosPrefab, paramList.transform);

                GameObject param = go.transform.Find("ParametroNome").gameObject;
                param.GetComponent<Text>().text = listaParametros[i].name;

                GameObject value = go.transform.Find("ParametroValor").gameObject;
                if (listaParametros[i].values.Length == 1) {
                    string stringValue = listaParametros[i].values[0].ToString();
                    value.GetComponent<Text>().text = float.Parse(stringValue, CultureInfo.InvariantCulture.NumberFormat).ToString("#.##");

                    GameObject unit = go.transform.Find("Unidade").gameObject;
                    unit.GetComponent<Text>().text = listaParametros[i].unit;
                }

                var imageObject =  go.GetComponent<ParameterManager>();
                imageObject.unit = listaParametros[i].name;

            }
        }
    }

    public void UpdateAlerts()
    {
        StartCoroutine(GetAlarmesRequest());
    }


    IEnumerator GetAlarmesRequest()
    {
        
        UnityWebRequest toHost = UnityWebRequest.Get(urlRoot + "/parameters/alertedParameters/public");
        Debug.Log(urlRoot + "/parameters/alertedParameters/public");
        yield return toHost.SendWebRequest();

        if (toHost.isNetworkError || toHost.isHttpError)
        {
            Debug.Log(toHost.error);
        }
        else
        {
            message.text = toHost.downloadHandler.text;
            Debug.Log(message.text);
            listaAlertas = DeserializeAlertas(message.text);
            
        }
    }

    IEnumerator GetParametrosRequest(string qr)
    {
        //Debug.Log(getURLbyQRCode + qr + "/public");

        URLTEXT.text = getURLbyQRCode + qr + "/public";

        UnityWebRequest toHost = UnityWebRequest.Get(getURLbyQRCode + qr + "/public");

        yield return toHost.SendWebRequest();

        if (toHost.isNetworkError || toHost.isHttpError)
        {
            Debug.Log(toHost.error);
        }
        else
        {
            message.text = toHost.downloadHandler.text;

            Debug.Log(message.text);

            listaParametros = DeserializeParametrosJson(message.text);

            UpdateList();
        }
    }

    IEnumerator GetAllComponentsRequest()
    {
        UnityWebRequest toHost = UnityWebRequest.Get(urlRoot + "/components/public");

        yield return toHost.SendWebRequest();

        if (toHost.isNetworkError || toHost.isHttpError)
        {
            Debug.Log(toHost.error);
        }
        else
        {
            message.text = toHost.downloadHandler.text;

            listaMaquinas = DeserializeAllComponents(message.text);
        }
    }



    public void OnButtonSendScore()
    {
        if (message.text == string.Empty)
        {
            textInputField.text = "Error: No high score to send. \nEnter a value in input field.";
        }
        else
        {
            textInputField.text = "Sending data...";
            StartCoroutine(SimplePostRequest(textInputField.text));
        }
    }

    IEnumerator SimplePostRequest(string dados)
    {
        List<IMultipartFormSection> Form = new List<IMultipartFormSection>();

        Form.Add(new MultipartFormDataSection("dadosKey", dados));

        UnityWebRequest postWebRequest = UnityWebRequest.Post(postURL, Form);

        yield return postWebRequest.SendWebRequest();

        if (postWebRequest.isNetworkError || postWebRequest.isHttpError)
        {
            Debug.LogError(postWebRequest.error);
        }
        else
        {
            message.text = postWebRequest.downloadHandler.text;
        }
    }

    #region Deserialize

    private List<JsonManagerApodi.Parametro> DeserializeParametrosJson(string JSON)
    {
        try
        {
            JsonManagerApodi.Equipment jObject = JsonConvert.DeserializeObject<JsonManagerApodi.Equipment> (JSON);
            //Debug.Log("Objeto: " + jObject.ToString());
            CurrentText = jObject.name;
            //Debug.Log(CurrentText);

           
            //return null;
            return jObject.parameters.OfType<JsonManagerApodi.Parametro>().ToList();
        }
        catch (Exception e)
        {
            Debug.Log("Erro de deserialização:" + e.Message.ToString());

            return null;
        }
    }

    private List<JsonManagerApodi.AlertedParameters> DeserializeAlertas(string JSON)
    {
        try
        {
            var jObject = JsonConvert.DeserializeObject<List<JsonManagerApodi.AlertedParameters>>(JSON);
            Debug.Log("Objeto: " + jObject.ToString());

            //return listaAvisos;
            return  jObject;
            
        }
        catch (Exception e)
        {
            Debug.Log("Erro de deserialização alertas:" + e.Message.ToString());

            return null;
        }
    }
    
    private List<JsonManagerApodi.Maquina> DeserializeAllComponents(string JSON)
    {
        List<JsonManagerApodi.Maquina> listaMaquinas = new List<JsonManagerApodi.Maquina>();
        try
        {
            var jObject = JsonConvert.DeserializeObject<dynamic>(JSON);
            //Debug.Log("Objeto: " + jObject.ToString());

            foreach (var maquina in jObject)
            {
                JsonManagerApodi.Maquina newMaquina = new JsonManagerApodi.Maquina(maquina.id, maquina.code, maquina.name, maquina.order, maquina.parent_id);
                foreach (var subcomponent in newMaquina.subComponents)
                {
                    //ARRUMAR PARA AMANHÃ
                    JsonManagerApodi.Maquina newSubMaquina = new JsonManagerApodi.Maquina(subcomponent.id, subcomponent.code, subcomponent.name, subcomponent.order, subcomponent.parent_id);
                    listaMaquinas.Add(newSubMaquina);
                }
                listaMaquinas.Add(newMaquina);
            }
            return listaMaquinas;
        }
        catch (Exception e)
        {
            Debug.Log("Erro de deserialização dos componentes:" + e.Message.ToString());

            return null;
        }
    }
    #endregion
}

