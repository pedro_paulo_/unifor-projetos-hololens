﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParameterManager : MonoBehaviour
{

    #region Biblioteca de ícones.

    [SerializeField]
    private Texture Image_Alerta_Outline_Amarelo;

    [SerializeField]
    private Texture Alerta_Outline_Vermelho;

    [SerializeField]
    private Texture Alerta_Preenchido_Amarelo;

    [SerializeField]
    private Texture Alerta_Preenchido_Vermelho;

    [SerializeField]
    private Texture Alerta_Preto_e_Amarelo;

    [SerializeField]
    private Texture Corrente_3_1;

    [SerializeField]
    private Texture Corrente_3_2;

    [SerializeField]
    private Texture Ppm_Gases_3_1;

    [SerializeField]
    private Texture Ppm_Gases_3_2;

    [SerializeField]
    private Texture Pressao_3_1;

    [SerializeField]
    private Texture Pressão_3_2;

    [SerializeField]
    private Texture Temperatura_3_1;

    [SerializeField]
    private Texture Temperatura_3_2;

    #endregion

    #region Parametros.
    
    private Text paramTex { get; set; }

    private Text paramValue { get; set; }

    public RawImage image;

    public RawImage backImage;

    #endregion

    [HideInInspector]
    public string unit;

    // Start is called before the first frame update
    void Start()
    {
        paramTex = GameObject.Find("ParametroNome").GetComponent<Text>();

        paramValue = GameObject.Find("ParametroValor").GetComponent<Text>();

        //unit = "°C";
    }

    // Update is called once per frame
    void Update()
    {
        if (unit == "°C")
        {
            image.texture = Temperatura_3_1;
            backImage.texture = Temperatura_3_2;
        }else if (unit == "mbar")
        {
            image.texture = Pressao_3_1;
            backImage.texture = Pressão_3_2;
        }
        if (unit == "Grau de enchimento (GE)")
        {
            image.texture = Temperatura_3_1;
            backImage.texture = Temperatura_3_2;
        }
        else if (unit == "Torque")
        {
            image.texture = Pressao_3_1;
            backImage.texture = Pressão_3_2;
        }
        else if (unit == "Rotação")
        {
            image.texture = Pressao_3_1;
            backImage.texture = Pressão_3_2;
        }
        else
        {
            image.texture = Ppm_Gases_3_1;
            backImage.texture = Ppm_Gases_3_2;
        }

    }

    public void AtualizarDados(string nome, string valor)
    {
        paramTex.text = nome;
        
        paramValue.text = valor;
    }

}
