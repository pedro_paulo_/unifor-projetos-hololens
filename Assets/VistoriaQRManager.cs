﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class VistoriaQRManager : MonoBehaviour
{
    public TMP_Text detalhesText;

    string text1;
    string text2;
    string text3;
    string text4;

    [SerializeField]
    private GameObject Imagens;

    int selected;
    // Start is called before the first frame update
    void Start()
    {

        text1 = "Verifique o equipamento.Uma causa comum de falha no transformador é o superaquecimento, o qual faz com que a " +
            "fiação interna do aparelho funcione em temperaturas elevadas. Isso geralmente causa uma deformação no aparelho ou no local onde está instalado." +
            "Se houver uma saliência na parte externa ou se tiver marcas de queimado no transformador, troque - o em vez de fazer um teste.";

        text2 = "Avalie a fiação do transformador.A fiação deve estar identificada no transformador de forma clara.No entanto, é importante obter um " +
            "esquema do circuito que contém o transformador para entender como ele é conectado. O esquema geralmente fica disponível no manual do" +
            " produto ou no site do fabricante do circuito[1].";

        text3 = "Descubra quais as saídas e entradas do transformador.O primeiro circuito elétrico será conectado ao enrolamento primário do transformador, " +
            "que é a entrada elétrica.O segundo circuito que recebe energia do transformador é conectado ao secundário, ou a saída.[2] A tensão abastecida " +
            "para o primário deve estar identificada no transformador e no esquema. A tensão gerada pelo secundário deve estar identificada da mesma forma " +
            "que no primário.";

        text4 = " Saiba qual é a filtragem de saída.É comum colocar capacitores e diodos no enrolamento secundário do transformador para converter" +
            " a corrente alternada da saída para corrente contínua. Essa informação não está disponível no rótulo do transformador.[3] Na maioria dos casos " +
            "é possível usar a conversão do transformador e a informação da filtragem de saída do esquema.";


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AtivarTextosEImagems()
    {
            
    }

    public void AtualizarTexto(string text)
    {
        if (text == "1")
        {
            detalhesText.text = text1;
        }else if (text == "2")
        {
            detalhesText.text = text2;
        }else if (text == "3")
        {
            detalhesText.text = text3;
        }else if (text == "4")
        {
            detalhesText.text = text4;
        }
    }
}
