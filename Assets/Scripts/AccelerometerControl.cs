﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class AccelerometerControl : MonoBehaviour
{
    [SerializeField]
    private GameObject window;
    [SerializeField]
    private GameObject observer;

    private Vector3 observerLocation;

    private Quaternion observerRotation;

    private float [] Accelerometer = new float[3];
    // Start is called before the first frame update

    [SerializeField]
    private TextMeshProUGUI text1;

    [SerializeField]
    private TextMeshProUGUI text2;

    [SerializeField]
    private TextMeshProUGUI text3;


    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Accelerometer[0] != 0 || Accelerometer[2] != 0)
        {

            text1.text = Accelerometer[0].ToString();
            text2.text = Accelerometer[1].ToString();
            text3.text = Accelerometer[2].ToString();

            //SetScreenOff();
        }

        if (Accelerometer[0] == 0 || Accelerometer[2] == 0)
        {
            text1.text = Accelerometer[0].ToString();
            text2.text = Accelerometer[1].ToString();
            text3.text = Accelerometer[2].ToString();

            SetScreenOn();
        }
    }

    void FixedUpdate()
    {
        UpdateAccelerometer();
    }


    void UpdateAccelerometer()
    {
        Accelerometer[0] = Input.acceleration.x;
        //Debug.Log("X = " + Accelerometer[0]);
        Accelerometer[1] = Input.acceleration.y;
        //Debug.Log("Y = " + Accelerometer[1]);
        Accelerometer[2] = Input.acceleration.z;
        //Debug.Log("Z = " + Accelerometer[2]);
    }

    void SetScreenOff()
    {
        SaveLocationRotation();
        window.SetActive(false);     
    }

    void SetScreenOn()
    {
        observerLocation =  UpdateLocation();
        observerRotation = UpdateRotation();

        window.SetActive(true);
    }

    void SaveLocationRotation()
    {
        observerLocation = observer.transform.position;
        observerRotation = observer.transform.rotation;
    }

    Quaternion UpdateRotation()
    {       
        Quaternion tempRotation;        
        tempRotation = observer.transform.rotation * Quaternion.Inverse(observerRotation);
        return tempRotation;
    }

    Vector3 UpdateLocation()
    {
        Vector3 tempLocation;
        tempLocation = observer.transform.position - observerLocation;
        return tempLocation;
    }

}
