﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ScreenManager : MonoBehaviour
{
    //Login Screen
    [SerializeField]
    private GameObject login;


    //Main Menu Screen
    [SerializeField]
    private GameObject mainMenu;
    [SerializeField]
    private GameObject mainMenuEmAndamento;
    //Vistorias/Subestacoes
    [SerializeField]
    public GameObject mainMenuSubestacoes;

    [SerializeField]
    public GameObject mainMenuConcluidas;
    [SerializeField]
    public GameObject mainMenuPendente;

    //Vistoria
    [SerializeField]
    public GameObject VistoriaEstacao;
    //Vistoria Equipamento
    [SerializeField]
    private GameObject DetalhesEquipamento;
    [SerializeField]
    private GameObject VistoriaEquipamento;
    [SerializeField]
    private GameObject VistoriaQR;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LogOut()
    {
        login.SetActive(true);
        mainMenu.SetActive(false);
        mainMenuEmAndamento.SetActive(false);
        mainMenuSubestacoes.SetActive(false);
        mainMenuConcluidas.SetActive(false);
        mainMenuPendente.SetActive(false);

    }

    public void Login_To_MainMenu()
    {
        login.SetActive(false);
        mainMenu.SetActive(true);
        mainMenuEmAndamento.SetActive(true);
        mainMenuConcluidas.SetActive(true);
        mainMenuPendente.SetActive(true);
    }

    public void EntrarEmVistoria()
    {
        VistoriaEstacao.SetActive(true);
        mainMenuSubestacoes.SetActive(false);
        mainMenuEmAndamento.SetActive(false);
        mainMenuConcluidas.SetActive(false);
        mainMenuPendente.SetActive(false);
    }

    public void SairVistoria()
    {
        VistoriaEstacao.SetActive(false);
        mainMenuSubestacoes.SetActive(true);
        mainMenuEmAndamento.SetActive(true);
        mainMenuConcluidas.SetActive(true);
        mainMenuPendente.SetActive(true);
    }

    public void VisualizarVistoriarEquipamento()
    {
        VistoriaEstacao.SetActive(false);
        DetalhesEquipamento.SetActive(true);
        mainMenuSubestacoes.SetActive(false);
        mainMenuEmAndamento.SetActive(false);
        mainMenuConcluidas.SetActive(false);
        mainMenuPendente.SetActive(false);
    }
    public void SairDaVisualizarVistoriaDoEquipamento()
    {
        DetalhesEquipamento.SetActive(false);
        VistoriaEstacao.SetActive(true);
        mainMenuSubestacoes.SetActive(false);
        mainMenuEmAndamento.SetActive(false);
        mainMenuConcluidas.SetActive(false);
        mainMenuPendente.SetActive(false);
    }

    public void VistoriarEquipamento()
    {
        VistoriaEquipamento.SetActive(true);
        DetalhesEquipamento.SetActive(false);
    }
    public void SairVistoriarEquipamento()
    {
        VistoriaEquipamento.SetActive(false);
        VistoriaEstacao.SetActive(true);
    }
    public void IniciarVistoriaQR()
    {
        VistoriaQR.SetActive(true);
        VistoriaEstacao.SetActive(false);
    }
    public void FinalizarVistoriaQR()
    {
        VistoriaQR.SetActive(false);
        VistoriaEstacao.SetActive(true);
    }
}