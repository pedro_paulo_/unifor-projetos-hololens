﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alarm : MonoBehaviour
{
    public Animator anim;


    private bool alarming = false;
    public bool warning = true;
    public int alarmType = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    public void StopAllAlarms()
    {
        alarmType = -1;
    }

    // Update is called once per frame
    void Update()
    {
        if (warning == true && alarming == false || warning == true && alarming == true)
        {
            alarming = true;
            ActiveAlarm();
        }
        if (warning == false && alarming == true)
        {
            alarming = false;
        }
    }

    void ActiveAlarm()
    {
        if (alarmType == 1)
        {
            anim.SetInteger("Type",1);         
        }
        else if (alarmType == 2)
        {   
            anim.SetInteger("Type",2);        
        }
        else
        {
            anim.SetInteger("Type",0);
        }
        
    }
}
