﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

public class UnityWebRequestAwaiter : INotifyCompletion
{
    private UnityWebRequestAsyncOperation asyncOp;
    private Action continuation;

    public UnityWebRequestAwaiter(UnityWebRequestAsyncOperation asyncOp)
    {
        this.asyncOp = asyncOp;
        asyncOp.completed += OnRequestCompleted;
    }

    public bool IsCompleted { get { return asyncOp.isDone; } }

    public void GetResult() { }

    public void OnCompleted(Action continuation)
    {
        this.continuation = continuation;
    }

    private void OnRequestCompleted(AsyncOperation obj)
    {
        continuation();
    }
}

public static class ExtensionMethods
{
    public static UnityWebRequestAwaiter GetAwaiter(this UnityWebRequestAsyncOperation asyncOp)
    {
        return new UnityWebRequestAwaiter(asyncOp);
    }
}

public class Client : MonoBehaviour
{
    public Text message;
    public InputField textInputField;

    [SerializeField]
    private GameObject subestacaoPrefab;
    [SerializeField]
    private GameObject vistoriaConcluidaPrefab;
    [SerializeField]
    private GameObject vistoriaAndamentoPrefab;
    [SerializeField]
    private GameObject vistoriaPendentePrefab;

    [SerializeField]
    private GameObject listaSubstacao;
    [SerializeField]
    private GameObject listaVistoriaAndamento;
    [SerializeField]
    private GameObject listaVistoriaPendente;
    [SerializeField]
    private GameObject listaVistoriaFinalizada;

    List<GameObject> prefbsList;

    public ScreenManager sm;

    public string qrCode = "";

    List<JsonManager.SubEstacaoData> substations;
    List<JsonManager.VistoriasData> vistorias;

    #region GameObjects Instaciados
    List<GameObject> gameObjectSubstationList;
    List<GameObject> gameObjectVisitationList;
    #endregion

    #region URL's
    //http://amora.unifor.br:3000/info/routes
    string urlRoot = "http://amora.unifor.br:3000";

    string getURLSubstations = "/api/v1/substations";

    string getURLVistorias = "/api/v1/visits";

    string username = "102030";
    string password = "12345678";

    JsonManager.AuthToken auth = new JsonManager.AuthToken();
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        //getURLSubstations = urlRoot + getURLSubstations;
        getURLVistorias = urlRoot + getURLVistorias;

        prefbsList = new List<GameObject>();
        gameObjectSubstationList = new List<GameObject>();
        gameObjectVisitationList = new List<GameObject>();

        substations = new List<JsonManager.SubEstacaoData>();
        vistorias = new List<JsonManager.VistoriasData>();
    }

    #region Login
    public void Login()
    {
        StartCoroutine(loginRequest());

    }

    public async Task<string> RequestTokensFromLogin(string URL)
    {
        Debug.Log("Auth "+ auth.access_token);
        Debug.Log("Auth " + auth.token_type);
        Debug.Log("Auth " + auth.client);
        Debug.Log("Auth " + auth.expiry);
        Debug.Log("Auth " + auth.uid);
        UnityWebRequest toHost = UnityWebRequest.Get(urlRoot + URL);
        toHost.SetRequestHeader("access-token", auth.access_token);
        toHost.SetRequestHeader("token-type", auth.token_type);
        toHost.SetRequestHeader("client", auth.client);
        toHost.SetRequestHeader("expiry", auth.expiry);
        toHost.SetRequestHeader("uid", auth.uid);

        await toHost.SendWebRequest();

        if (toHost.isNetworkError || toHost.isHttpError)
        {
            Debug.Log(toHost.error);
            return "Error";
        }
        else
        {
            return toHost.downloadHandler.text;
        }
       
        
    }

    IEnumerator loginRequest()
    {
        // http://unifor.amora.br:3000/api/v1/auth/sign_in
        WWWForm form = new WWWForm();
        form.AddField("registration", username);
        form.AddField("password", password);

        UnityWebRequest toHost = UnityWebRequest.Post(urlRoot + "/api/v1/auth/sign_in", form);
        toHost.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        yield return toHost.SendWebRequest();

        if (toHost.isNetworkError || toHost.isHttpError)
        {
            message.text += "\nLogin Error";
            Debug.Log(toHost.error);
        }
        else
        {
            var dict = toHost.GetResponseHeaders();
            /*
            foreach (KeyValuePair<string, string> e in dict)
            {
                Debug.Log("TOKEN:" + e.Key); 
                Debug.Log("TOKEN:" + e.Value);
            }*/
            
            auth.access_token = dict["access-token"];
            auth.token_type = dict["token-type"];
            auth.client = dict["client"];
            auth.expiry = dict["expiry"];
            auth.uid = dict["uid"];
            // TODO mudar a cena e chamar rota de alertas
            Debug.Log("Login COmplet");
            message.text += "\nLogin complete";

            GetSubstations();

        }

    }

    public void CarregarElementosIniciar()
    {
        GetSubstations();
        GetVerifications();
    }
    #endregion

    void UpdateSubstationList()
    {
        /*
        substations.Add(new JsonManager.SubEstacaoData {
            id ="0001",
            name ="Estacao 01",
            regional= "regional",
            sectional= "sectional1"
        });
        substations.Add(new JsonManager.SubEstacaoData
        {
            id = "0002",
            name = "Estacao 02",
            regional = "regional",
            sectional = "sectional1"
        });
        substations.Add(new JsonManager.SubEstacaoData
        {
            id = "0003",
            name = "Estacao 03",
            regional = "regional3333",
            sectional = "sectional1333"
        });
        */

        if (substations != null)
        {
            foreach (JsonManager.SubEstacaoData i in substations)
            {
                message.text += "\n\n\nPushing new GO";
                GameObject go = Instantiate(subestacaoPrefab, listaSubstacao.transform);
                //go.transform.SetParent(listaSubstacao.transform, false);
                message.text += "\nSetting iD " + i.id;
                GameObject id = go.transform.Find("IDEstacaoText").gameObject;
                id.GetComponent<Text>().text = (string)i.id;

                message.text += "\nSetting NAME " + i.name;
                GameObject name = go.transform.Find("SubstacaoNameTex").gameObject;
                name.GetComponent<Text>().text = (string)i.name;
                message.text += "\nSetting Regional " + i.regional;
                GameObject regional = go.transform.Find("RegionalTex").gameObject;
                regional.GetComponent<Text>().text = (string)i.regional;
                message.text += "\nSetting Sectrion " + i.sectional;
                GameObject seccional = go.transform.Find("SeccionalTex").gameObject;
                seccional.GetComponent<Text>().text = (string)i.sectional;
                message.text += "\nSetting Done station";
                //      gameObjectSubstationList.Add(go);

            }
        }
        
    }

    void UpdateVistoriasLists()
    {
        if (vistorias != null)
        {
            foreach(dynamic i in vistorias)
            {
                if (i.started_at == null) {
                    //Pendente
                   GameObject go0 = Instantiate(vistoriaPendentePrefab, listaVistoriaPendente.transform);
                   // go0.transform.SetParent(this.listaVistoriaPendente.transform);

                    GameObject id = go0.transform.Find("IDText").gameObject;
                    id.GetComponent<Text>().text = i.id;

                    GameObject name = go0.transform.Find("SubstacaoTex").gameObject;
                    name.GetComponent<Text>().text = i.substation.name;

                    go0.GetComponent<Button>().onClick.AddListener(() => sm.EntrarEmVistoria());

                    gameObjectVisitationList.Add(go0);
                }
                else if (i.started_at != null || i.ended_at == null)
                {
                    //Andamento
                    GameObject go1 = Instantiate(vistoriaAndamentoPrefab, listaVistoriaAndamento.transform);
                    //go1.transform.SetParent(this.listaVistoriaAndamento.transform);

                    GameObject id = go1.transform.Find("IDText").gameObject;
                    id.GetComponent<Text>().text = i.id;

                    GameObject name = go1.transform.Find("SubstacaoTex").gameObject;
                    name.GetComponent<Text>().text = i.substation.name;

                    GameObject regional = go1.transform.Find("InicioTex").gameObject;
                    regional.GetComponent<Text>().text = i.started_at;

                    go1.GetComponent<Button>().onClick.AddListener(() => sm.EntrarEmVistoria());

                    gameObjectVisitationList.Add(go1);
                }
                else if (i.started_at != null || i.ended_at != null)
                {
                    //Finalizada
                    GameObject go2 = Instantiate(vistoriaConcluidaPrefab, listaVistoriaFinalizada.transform);
                    //go2.transform.SetParent(this.listaVistoriaFinalizada.transform);

                    GameObject id = go2.transform.Find("IDText").gameObject;
                    id.GetComponent<Text>().text = i.id;

                    GameObject name = go2.transform.Find("SubstacaoTex").gameObject;
                    name.GetComponent<Text>().text = i.substation.name;

                    GameObject regional = go2.transform.Find("InicioTex").gameObject;
                    regional.GetComponent<Text>().text = i.started_at;

                    GameObject seccional = go2.transform.Find("DataFimTex").gameObject;
                    seccional.GetComponent<Text>().text = i.ended_at;

                    go2.GetComponent<Button>().onClick.AddListener(() => sm.EntrarEmVistoria());

                    gameObjectVisitationList.Add(go2);

                }
                
            }
        }
    }

    #region Gets

    public async void GetSubstations()
    {
        message.text += "\natualizando dados...";
        try
        {
            var data = (string)await RequestTokensFromLogin(getURLSubstations);
            if (data != "")
            {
                Debug.Log("Starting...");
                message.text += "\nStarting";
                message.text += "\nDATA: " + data;
                //var list = JsonManager.JsonHelper.FromJson<JsonManager.SubEstacaoData>(data);
                IEnumerable<JsonManager.SubEstacaoData> list = JsonConvert.DeserializeObject<IEnumerable<JsonManager.SubEstacaoData>>(data);
                Debug.Log("Complete...");
                message.text += "\nComplete";
                substations.Clear();
                message.text += "\nList Cleared";
                substations.AddRange(list);
                message.text += "\nAdd range";
                foreach (JsonManager.SubEstacaoData J in substations)
                {
                    Debug.Log("Item " + J.id);
                    Debug.Log("Name " + J.name);
                    message.text += "\nAdded "+J.name;
                }
                UpdateSubstationList();
            }
            Debug.Log("Result await:" + data);
        } catch {

        }
        
    }

    public void GetVerifications()
    {
        message.text = "atualizando dados...";

        StartCoroutine(GetRequest(getURLVistorias));
    }

    IEnumerator GetRequest(string s)
    {
        UnityWebRequest toHost = UnityWebRequest.Get(s);

        yield return toHost.SendWebRequest();

        if (toHost.isNetworkError || toHost.isHttpError)
        {
            Debug.Log(toHost.error);
        }
        else
        {

            message.text = toHost.downloadHandler.text;

            if (s == getURLSubstations)
            {
                deserializeSubstationsJSON(toHost.downloadHandler.text);
                UpdateSubstationList();
            }
            else if (s == getURLVistorias)
            {
                //deserializeVisotirasJSON(message.text);
                //UpdateVistoriasLists();
            }

        }
    }
    #endregion


    #region Posts
    public void OnButtonSendScore()
    {
        if (message.text == string.Empty)
        {
            textInputField.text = "Error: No high score to send. \nEnter a value in input field.";
        }
        else
        {
            textInputField.text = "Sending data...";
            //StartCoroutine(SimplePostRequest(textInputField.text));
        }
    }

    /*IEnumerator SimplePostRequest(string dados)
    {
        List<IMultipartFormSection> Form = new List<IMultipartFormSection>();

        Form.Add(new MultipartFormDataSection("dadosKey", dados));

        UnityWebRequest postWebRequest = UnityWebRequest.Post(postURL, Form);

        yield return postWebRequest.SendWebRequest();

        if (postWebRequest.isNetworkError || postWebRequest.isHttpError)
        {
            Debug.LogError(postWebRequest.error);
        }
        else
        {
            message.text = postWebRequest.downloadHandler.text;
        }
    }
    */
    
    #endregion

    #region Desserialização
    private void deserializeSubstationsJSON(string JSON)
    {  
        try
        {
            var jObject = JsonConvert.DeserializeObject<dynamic>(JSON);
            //dynamic jObject = JsonUtility.FromJson<JsonManager.SubEstacaoData>(JSON);
            substations.Clear();
            Debug.Log("Objeto: " + jObject.ToString());
            foreach (var instance in jObject)
            {
                    JsonManager.SubEstacaoData newStation = new JsonManager.SubEstacaoData((string)instance.id, (string)instance.name, (string)instance.sicode, (string)instance.sectional,
                    (string)instance.regional, instance.lat, instance.longe, (DateTime)instance.created_at, (DateTime)instance.updated_at); 
                    substations.Add(newStation);
            }
            //return substations;
            
        }
        catch(Exception e)
        {
            Debug.Log("Erro de deserialização:" + e.Message.ToString());
            //return null;
        }
    }

    private void deserializeVisotirasJSON(string JSON)
    {
        
        try
        {
            var jObject = JsonConvert.DeserializeObject<dynamic>(JSON);
            Debug.Log("Objeto: " + jObject.ToString());
            vistorias.Clear();
            foreach (var instance in jObject)
            {
                    JsonManager.VistoriasData newVisit = new JsonManager.VistoriasData(instance.id, instance.initialstension, instance.inspectiontype,
                    instance.inspectionorder, instance.rec, instance.visitsi, instance.started_at, instance.ended_at, instance.name, instance.substation);
                    vistorias.Add(newVisit);

            }
        }

        catch (Exception e)
        {
            Debug.Log("Erro de deserialização:" + e.Message.ToString());
            //return null;
        }
    }
    


    #endregion
}
