﻿    using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JsonManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static class JsonHelper
    {
        public static T[] FromJson<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.Items;
        }

        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper);
        }

        public static string ToJson<T>(T[] array, bool prettyPrint)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper, prettyPrint);
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }
    }

    [Serializable]
    public class AuthToken
    {
        public string access_token;
        public string token_type;
        public string client;
        public string expiry;
        public string uid;
    }

    [Serializable]
    public class SubEstacaoData
    {
        public string id;
        public string name;
        public string sicode;
        public string sectional;
        public string regional;
        public dynamic lat;
        public dynamic longe;
        public DateTime created_at;
        public DateTime updated_at;

        
        public SubEstacaoData()
        {
        }

        public SubEstacaoData(string id, string name, string sicode, string sectional, string regional, dynamic lat, dynamic longe, DateTime created_at, DateTime updated_at)
        {
            this.id = id;
            this.name = name;
            this.sicode = sicode;
            this.sectional = sectional;
            this.regional = regional;
            this.lat = lat;
            this.longe = longe;
            this.created_at = created_at;
            this.updated_at = updated_at;
        }
    }

    [Serializable]
    public class VistoriasData
    {
        public dynamic id;
        public dynamic initialstension;
        public dynamic inspectiontype;
        public dynamic inspectionorder;
        public dynamic rec;
        public dynamic visitsi;
        public dynamic started_at;
        public dynamic ended_at;
        public dynamic name;
        public dynamic substation;

        public VistoriasData(dynamic id, dynamic initialstension, dynamic inspectiontype, dynamic inspectionorder, dynamic rec, dynamic visitsi, dynamic started_at, dynamic ended_at, dynamic name, dynamic substation)
        {
            this.id = id;
            this.initialstension = initialstension;
            this.inspectiontype = inspectiontype;
            this.inspectionorder = inspectionorder;
            this.rec = rec;
            this.visitsi = visitsi;
            this.started_at = started_at;
            this.ended_at = ended_at;
            this.name = name;
            this.substation = substation;
        }

        public VistoriasData()
        {

        }
    }
}
